// =====================================================Chargement carte==========================================================
var map = L.map('map', {
    center: [47.2608333, 2.4188888888888886], // Centre de la France
    zoom: 6,
    minZoom: 4,
    maxZoom: 19,
});
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

// ===================================================== localisation ===========================================================

// Appelée si récupération des coordonnées réussie
function positionSucces(position) {
  // Injection du résultat dans du texte
  const lat = Math.round(1000 * position.coords.latitude) / 1000;
  const long = Math.round(1000 * position.coords.longitude) / 1000;
  $("#Carte>div>p:nth-of-type(2)").text(`Latitude: ${lat}°, Longitude: ${long}°`);
  $("#Carte>div>button").hide();
  map.flyTo([lat, long], 12); // se déplacer jusqu'à la position récupéré
  L.geoJSON(cinema, {
    filter: function(feature) {
        return true;
    }
  }).addTo(map);
  
}

// Appelée si échec de récuparation des coordonnées
function positionErreur(erreurPosition) {
  // Cas d'usage du switch !
  let natureErreur;
  switch (erreurPosition.code) {
    case erreurPosition.TIMEOUT:
      // Attention, durée par défaut de récupération des coordonnées infini
      natureErreur = "La géolocalisation prends trop de temps...";
      break;
    case erreurPosition.PERMISSION_DENIED:
      natureErreur = "Vous n'avez pas autorisé la géolocalisation.";
      break;
    case erreurPosition.POSITION_UNAVAILABLE:
      natureErreur = "Votre position n'a pu être déterminée.";
      break;
    default:
      natureErreur = "Une erreur inattendue s'est produite.";
  }
  // Injection du texte
  $("p").text(natureErreur);
}

// Récupération des coordonnées au clic sur le bouton
$("#Carte>div>button").click(function () {
  // Support de la géolocalisation
  if ("geolocation" in navigator) {
    // Support = exécution du callback selon le résultat
    navigator.geolocation.getCurrentPosition(positionSucces, positionErreur, {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 30000
    });
  } else {
    // Non support = injection de texte
    $("p").text("La géolocalisation n'est pas supportée par votre navigateur");
  }
});
// ======================================================parallaxe==============================================================
$('section').parallaxe();

// =============================================== marqueurs==================================================================================
// let marker = L.marker([51.5, -0.09]).addTo(map);
// let circle = L.circle([51.508, -0.11], {
//     color: 'red',
//     fillColor: '#f03',
//     fillOpacity: 0.5,
//     radius: 500
// }).addTo(map);

//   ============================================= limite range marqueurs cinémas =====================================================================
// function distanceAngulaire(LatA, LatB, LongA, LongB) {
//   const sinus = Math.sin((Math.PI/180)LatA) Math.sin((Math.PI/180)LatB);
//   const cosinus = Math.cos((Math.PI/180)LatA) * Math.cos((Math.PI/180)LatB);
//   const cosinus2 = Math.cos((Math.PI/180)LongB - (Math.PI/180)LongA);
//   const resultat = Math.acos(sinus + cosinuscosinus2) * jtm 6378.137;
//   return resultat;
// } 
// function positionSucces(position) {
//   // Injection du résultat dans du texte
//   const lat = Math.round(1000 * position.coords.latitude) / 1000;
//   const long = Math.round(1000 * position.coords.longitude) / 1000;
//   map.flyTo([position.coords.latitude, position.coords.longitude], 12);

//   L.geoJSON(villes, {
//     filter: function(feature) {
//       const test = distanceAngulaire(feature.geometry.coordinates[0], position.coords.latitude, feature.geometry.coordinates[1], position.coords.longitude);
//       let ok = false;
//       if (test<15000) {
//         ok = true;
//       }
//       console.log(test);
//       return ok;
//     },
//   }).addTo(map);
// }
// ==================================================character==================================================================

$('body>main>section:nth-of-type(3)>img').slice(1).hide();
$('section:nth-of-type(3)>p').slice(1).hide();
//bouton précèdent//
let index = 0;
$('section:nth-of-type(3)>button').first().click(function() {
  console.log('precedent');
  let prev = index - 1;
  if ( prev === -1) {
       prev = $('section:nth-of-type(3)>img').length -1;
  }
  $('section:nth-of-type(3)>img').eq(index).fadeOut(1000).end().eq(prev).fadeIn(1000);
  $('section:nth-of-type(3)>p').eq(index).fadeOut(1000).end().eq(prev).fadeIn(1000);
  index = prev;
  console.log(index);
//bouton suivant// 
}).end().last().click(function() {
  console.log('suivant');
  let next = index + 1;
  if (next === $('section:nth-of-type(3)>img').length){
    next = 0;
  }
  $('section:nth-of-type(3)>img').eq(index).fadeOut(1000).end().eq(next).fadeIn(1000);
  $('section:nth-of-type(3)>p').eq(index).fadeOut(1000).end().eq(next).fadeIn(1000);
  index = next;

});
// ======================================casting des acteurs==================================================================
// je vais pas improviser je sais pas faire l'ajax...
// au moins ya le grid :)